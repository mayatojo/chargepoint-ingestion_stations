import json
import boto3
from zeep import Client
from zeep.wsse.username import UsernameToken
import pandas as pd
import time

from util.aws_s3 import upload_to_s3, connect_s3, get_secret

# ----------------------------------------------------------------------- SETUP
start = time.time()
s3_client = boto3.client('s3')
s3_resource = connect_s3(region_name='eu-west-2')
s3_bucket = 'ev-modeling-platform'
s3_meta_codes = '0_ReferenceData/MetaDataForCode/ChargePoint_postalCodes.json'
s3_tmp_stations = '2_TempData/ChargePoint_Stations/stations.csv'

secret_name = 'ChargePoint_API'
secret_region = 'eu-central-1'

def get_stations(client, postalCodes, km=350):
    """
    postalCodes: list of postal codes for API, format like 'NE61 3JN'
    """
    radius = int(km/1.609344)  # proximityUnit K does not work, is still M

    all_stations = []
    for postalCode in postalCodes:
        searchQuery = {'postalCode': postalCode,
                       'Proximity': radius, 'proximityUnit': 'M'}

        stations = client.service.getStations(searchQuery)
        all_stations += stations.stationData

    infos = []
    for station in all_stations:
        infos.append((station.stationID, station.stationActivationDate,
                      station.postalCode))
    short = list(dict.fromkeys(infos))

    stations_df = pd.DataFrame(short)
    stations_df.rename(columns={0: 'stationID',
                                1: 'activationDate',
                                2: 'ZipCode'},
                       inplace=True)
    unique_codes = list(stations_df.ZipCode)
    return {'codes': unique_codes}, stations_df

def main():
    # --------------------------------------------------------------- API SETUP
    # insert your organization's username and password below
    secret = get_secret(secret_name=secret_name, region_name=secret_region)
    username = secret['username']
    password = secret['password']

    # connection to client, set wsse auth
    wsdl_url = secret['wsdl_url']
    print('INFO: got secrets ', wsdl_url,
          'elapsed time (minutes): ', (time.time() - start) / 60)
    client = Client(wsdl_url, wsse=UsernameToken(username, password))

    # ---------------------------------------------------------------- DOWNLOAD
    try:
        meta_bytes = s3_client.get_object(Bucket=s3_bucket,
                                          Key=s3_meta_codes)
        file_content = meta_bytes['Body'].read().decode('utf-8')
        codes_json = json.loads(file_content)
        postalCodes = codes_json['codes']
        msg = 'Update existing file.'
        print('INFO: update existing file of postalCodes, has codes: '
              , len(postalCodes),
              'elapsed time (minutes): ', (time.time() - start) / 60)

    except :
        msg = 'Create new file.'
        stations = client.service.getStations({})
        codes = []
        for station in stations.stationData:
            codes.append(station.postalCode)
        postalCodes = list(dict.fromkeys(codes))
        print('INFO: create new file from default response, has codes: '
              , len(postalCodes),
              'elapsed time (minutes): ', (time.time() - start) / 60)

    updated_postalCodes, stations_df = get_stations(client, postalCodes, km=350)
    print('INFO: finished download. next: upload',
          'elapsed time (minutes): ', (time.time() - start) / 60)
    print('INFO: postalCodes: ', len(updated_postalCodes['codes']),
          'stations: ', stations_df.shape[0])

    # ------------------------------------------------------------ UPLOAD TO S3
    bucket = 'ev-modeling-platform'
    # ------------------------------------------------------- UPDATED ZIP-CODES
    print('INFO: upload postalCodes to: ', s3_meta_codes)
    uploadByteStream = bytes(json.dumps(updated_postalCodes).encode('UTF-8'))
    upload_to_s3(s3_resource, s3_bucket=bucket, s3_location=s3_meta_codes,
                 body=uploadByteStream)
    # ------------------------------------------ TABLE WITH STATION INFORMATION
    print('INFO: upload statioin information to: ', s3_tmp_stations)
    uploadByteStream = bytes(stations_df.to_csv().encode('UTF-8'))
    upload_to_s3(s3_resource, s3_bucket=bucket, s3_location=s3_tmp_stations,
                 body=uploadByteStream)

    return {'message': '{} Found {} unique Zip Codes.'
        .format(msg, len(updated_postalCodes['codes']))}
        

if __name__ == "__main__":
    main()