import json
import boto3
import base64
from botocore.exceptions import ClientError

bucket = 'ev-modeling-platform'


def connect_s3(region_name):
    """ connects to aws as indentified in credentials.json
        return: session.resource('s3')
    """
    session = boto3.Session(region_name=region_name)
    s3 = session.resource('s3')
    return s3


def get_files_in_folder(s3_resource, folder='data',
                        bucket=bucket):
    bucket = s3_resource.Bucket(bucket)

    keys = []
    for obj in bucket.objects.all():
        if obj.key.startswith(folder):
            keys.append(obj.key)

    return keys


def upload_to_s3(s3_resource, s3_bucket, s3_location, body):
    """ uploads local file to s3
    """
    s3_resource.Object(s3_bucket, s3_location).put(Body=body)


def delete_file(s3_resource, s3_location, bucket=bucket):
    """ deletes file from s3
    """
    s3_resource.Object(bucket, s3_location).delete()


def download_file(s3_resource, s3_location, local_location,
                  bucket='mayato-geo-pilot'):
    try:
        s3_resource.Bucket(bucket).download_file(s3_location, local_location)
    except ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise


def get_secret(secret_name, region_name='eu-central-1'):
    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    # In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
    # See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
    # We rethrow the exception by default.

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        else:
            print(e.response)

    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = json.loads(get_secret_value_response['SecretString'])
        else:
            secret = json.loads(
                base64.b64decode(get_secret_value_response['SecretBinary']))
        return secret