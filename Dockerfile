FROM python:3


COPY requirements.txt ./
RUN pip freeze > requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install boto3
RUN pip install pandas
RUN pip install zeep

RUN mkdir /src

COPY lambda_function.py /src/lambda_function.py
COPY util /src/util

CMD ["python", "/src/lambda_function.py"]